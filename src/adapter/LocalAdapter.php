<?php
declare(strict_types=1);
/**
 * Notes:
 * User: ysian
 * Time: 21:46
 * @return
 */

namespace Ysian\Storage\adapter;

use Ysian\Storage\exception\StorageException;

class LocalAdapter extends AdapterAbstract
{
    use FileInfo;
    /**
     * @inheritDoc
     */
    public function uploadFile(array $options = [])
    {
        $result   = []; //返回数据
        $saveData = []; //新增数据
        foreach ($this->files as $key => $file) {
            $fileInfo   = $this->fileInfo($file);
            $this->checkFile($fileInfo->extention);
            $hash       = hash_file($this->hashType, $fileInfo->pathName);
            $temp       = $this->getAttamentInfo($hash);
            if (empty($temp)) {
                $rootPath   = $this->config['root'] ?? dirname(__DIR__,5);
                $secondPath = $this->config['dirname'];
                $basePath   = $rootPath.'/public/'.$secondPath;
                if (!$this->createDir($basePath))  throw new StorageException('文件夹创建失败，请核查是否有对应权限。');

                $baseUrl  = $this->config['domain'].'/'.$secondPath.'/';
                $saveName = $hash . '.' . $fileInfo->extention;
                $savePath = $basePath . '/' . $saveName;

                $temp = [
                    'origin_name' => $fileInfo->originName,
                    'name'        => $saveName,
                    'save_path'   => $savePath,
                    'url'         => $baseUrl.$saveName,
                    'mime'        => $fileInfo->mimeType,
                    'extension'   => $fileInfo->extention,
                    'size'        => $fileInfo->size,
                    'hash'        => $hash,
                    'hash_type'   => $this->hashType,
                    'driver'      => 'local',
                ];

                if (in_array($fileInfo->extention,$this->imgExtentionArr)) {
                    list($image_with, $image_height, $mime_type) = $this->file($fileInfo->pathName);
                    $temp['width']  = $image_with;
                    $temp['height'] = $image_height;
                    $temp['mime']   = $mime_type;
                }


                $file->move($savePath);
                array_push($saveData, $temp);
            }
            array_push($result, $temp);
        }
        # 保存数据
        $this->saveAttament($saveData);
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function uploadServerFile(string $file_path)
    {
        // TODO: Implement uploadServerFile() method.
    }

    /**
     * @inheritDoc
     */
    public function uploadBase64(string $base64, string $extension = 'png')
    {
        // TODO: Implement uploadBase64() method.
    }

    /**
     * @desc: createDir 描述
     */
    protected function createDir(string $path): bool
    {
        if (is_dir($path))  return true;

        $parent = dirname($path);
        if (!is_dir($parent)) {
            if (!$this->createDir($parent)) {
                return false;
            }
        }

        return mkdir($path);
    }

}