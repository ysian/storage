<?php
/**
 * @desc
 * @user: ysian
 * @date: 2023/3/8 22:11
 * @return
 */

namespace Ysian\Storage\adapter;

/**
 * 上传文件相关信息
 * @property string size        文件大小
 * @property string pathName    文件路径
 * @property string mimeType    文件类型
 * @property string extention   文件后缀
 * @property string originName  文件原名
 */

trait FileInfo
{
    public string $size;
    public string $pathName;
    public string $mimeType;
    public string $extention;
    public string $originName;
    public function fileInfo($file)
    {
        if( PHP_SAPI=='cli') {
            return $this->cliFileInfo($file);
        } else {
            return $this->fpmFileInfo($file);
        }
    }

    public function cliFileInfo($file)
    {
        /**
         * @var \Stringable $file
         */
        $this->size       = $file->getSize();
        $this->pathName   = $file->getPathname();
        $this->mimeType   = $file->getType();
        $this->extention  = $file->getUploadExtension();
        $this->originName = $file->getUploadName();
        return $this;
    }

    public function fpmFileInfo($file)
    {

    }


}