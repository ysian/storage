<?php
/**
 * Notes:
 * User: ysian
 * Time: 20:35
 * @return
 */

namespace Ysian\Storage\adapter;

interface AdapterInterface
{
    /**
     * @desc上传文件
     * @param array $options
     * @return mixed
     */
    public function uploadFile(array $options);

    /**
     * @desc 上传服务端文件
     * @param string $file_path
     * @return mixed
     */
    public function uploadServerFile(string $file_path);

    /**
     * 上传base文件
     * @param string $base64
     * @param string $extension
     * @return mixed
     */
    public function uploadBase64(string $base64, string $extension = 'png');
}