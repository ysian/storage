<?php

return [
    'default' => 'local', // local：本地 oss：阿里云 cos：腾讯云 qos：七牛云
    # 通用配置
    'common'  => [
        'single_limit' => 1024 * 1024 * 200, // 单个文件的大小限制，默认200M 1024 * 1024 * 200
        'total_limit' => 1024 * 1024 * 200, // 所有文件的大小限制，默认200M 1024 * 1024 * 200
        'nums' => 10, // 文件数量限制，默认10
        'include' => [], // 被允许的文件类型列表
        'exclude' => [], // 不被允许的文件类型列表
        'dirname' => function () {
            return date('Ymd');
        },
        'algo'    => 'md5'
    ],
    // 本地对象存储
    'local' => [
        'root' => dirname(__DIR__),
        'domain' => 'http://127.0.0.1:8787',
    ],
    // 阿里云对象存储
    'oss' => [
        'accessKeyId' => 'xxxxxxxxxxxx',
        'accessKeySecret' => 'xxxxxxxxxxxx',
        'bucket' => 'resty-webman',
        'domain' => 'http://webman.oss.tinywan.com',
        'endpoint' => 'oss-cn-hangzhou.aliyuncs.com',
    ],
    // 腾讯云对象存储
    'cos' => [
        'secretId' => 'xxxxxxxxxxxxx',
        'secretKey' => 'xxxxxxxxxxxx',
        'bucket' => 'resty-webman-xxxxxxxxx',
        'domain' => 'http://webman.oss.tinywan.com',
        'region' => 'ap-shanghai',
    ],
    // 七牛云对象存储
    'qos' => [
        'accessKey' => 'xxxxxxxxxxxxx',
        'secretKey' => 'xxxxxxxxxxxxx',
        'bucket' => 'resty-webman',
        'domain' => 'http://webman.oss.tinywan.com',
    ]
];